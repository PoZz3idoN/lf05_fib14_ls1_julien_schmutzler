import java.util.ArrayList;
import java.util.Scanner;

public class Raumschiff {
	private String name;
	private int energieversorgung, huelle, schutzschild, lebenserhaltungssysteme, anzahlReperaturAndroiden;
	private Scanner BroadcastKommunikator;
	private String Logbucheintr�ge;
	
	int torpedos = 0;
	Scanner scanner;
	ArrayList<Ladung> ladung = new ArrayList<Ladung>();
	ArrayList<String> BroadCastKommunikatorNachrichten = new ArrayList<String>();
	
	public void torpedoSchie�en(Raumschiff raumschiff) {
		if(torpedos == 0) {
			System.out.println("Keine Torpedos vorhanden");
		}else {
			System.out.println("Torpedo abgeschossen.");
			torpedos--;
			treffer();
		}
	}
	
	public void ladungsverzeichnisAusgeben() {
		for(int i = 0; i < ladung.size(); i++) {
			System.out.print(ladung.get(i).toString());
		}
	}
	
	public void phasenKanonen(Raumschiff raumschiff) {
		if(energieversorgung < 50) {
			System.out.println("Energieversorgung unter 50%");
		}else {
			System.out.println("Kanone abgeschossen.");
			treffer();
		}
	}
	
	public void treffer() {
		
	}
	
	private void schiffGetroffen() {
		if(schutzschild > 0) {
			schutzschild--;
		} else if(schutzschild < 0) {
			huelle--;
		} else if(huelle < 0) {
			System.out.println("Lebenserhaltungssysteme vernichtet.\n");
			SpielEnde();
		}
	}
	public void SpielEnde() {
		System.out.println("Schiff zerst�rt");
		//TODO Raumschiff remove
	}
	public void sendeBroadcast() {
		BroadcastKommunikator = scanner;
		System.out.println(BroadcastKommunikator);
	}
	public void beladeRaumschiff() {
		//this.add(ArrayList<Ladung>().getLadung());	//Erschaffe Ladung TODO
	}
	public void erhalteLogbucheintr�ge() {
		System.out.println(Logbucheintr�ge + "\n");
	}
	public void torpedosLaden() {
		torpedos++;
		System.out.println("Torpedos Geladen.\n Aktuelle Torpedos: " + torpedos + "\n");
	}
	public void sendeReperaturauftragSingle() {
		
	}
	public void sendeReperaturauftragAll() {
		
	}
	
	public void statusEinsehen() {
		System.out.println("Name: \n" + name + "Huelle: \n" + huelle + "Schutzschild: \n" + schutzschild + "Ladung: \n" + 
							ladung+ "Energieversorgung: \n" + "Lebenserhaltungssysteme: \n" + lebenserhaltungssysteme);
	}
	Raumschiff(){//Konstruktor - basic
		this.name = "Korvette " + Math.random()*100; //Math.random damit die namen (praktisch) niemals ausgehen S // TODO For-schleife.
		this.huelle = 10;
		this.schutzschild = 10;
		this.energieversorgung = 20;
		this.lebenserhaltungssysteme = 1;
		this.anzahlReperaturAndroiden = 0;
	}
	Raumschiff(String name, int huelle, int torpedos,int schutzschild, int energieversorgung, int lebenserhaltungssysteme, int anzahlReperaturAndroiden, ArrayList<Ladung> ladungsverzeichnis) {			//KONSTRUKTOR 2
		this.name = name;
		this.huelle = huelle;
		this.schutzschild = schutzschild;
		this.energieversorgung = energieversorgung;
		this.lebenserhaltungssysteme = lebenserhaltungssysteme;
		this.anzahlReperaturAndroiden = anzahlReperaturAndroiden;
		this.ladung = ladungsverzeichnis;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getEnergieversorgung() {
		return energieversorgung;
	}
	public void setEnergieversorgung(int energieversorgung) {
		this.energieversorgung = energieversorgung;
	}
	public int getHuelle() {
		return huelle;
	}
	public void setHuelle(int huelle) {
		this.huelle = huelle;
	}
	public int getSchutzschild() {
		return schutzschild;
	}
	public void setSchutzschild(int schutzschild) {
		this.schutzschild = schutzschild;
	}
	public int getLebenserhaltungssysteme() {
		return lebenserhaltungssysteme;
	}
	public void setLebenserhaltungssysteme(int lebenserhaltungssysteme) {
		this.lebenserhaltungssysteme = lebenserhaltungssysteme;
	}
	public int getAnzahlReperaturAndroiden() {
		return anzahlReperaturAndroiden;
	}
	public void setAnzahlReperaturAndroiden(int anzahlReperaturAndroiden) {
		this.anzahlReperaturAndroiden = anzahlReperaturAndroiden;
	}
//	public String getBroadcastKommunikator() {
//		return BroadcastKommunikator;
//	}
//	public void setBroadcastKommunikator(String broadcastKommunikator) {
//		BroadcastKommunikator = broadcastKommunikator;
//	}
	public ArrayList<Ladung> getLadung() {
		return ladung;
	}
	public void setLadung(ArrayList<Ladung> ladung) {
		this.ladung = ladung;
	}
	public ArrayList<String> getBroadCastKommunikatorNachrichten() {
		return BroadCastKommunikatorNachrichten;
	}
	public void setBroadCastKommunikatorNachrichten(ArrayList<String> broadCastKommunikatorNachrichten) {
		BroadCastKommunikatorNachrichten = broadCastKommunikatorNachrichten;
	}
	
	
}
