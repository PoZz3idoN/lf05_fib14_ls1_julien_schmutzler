
public class Sparbuch {
	private int Kontonummer, Kapital, Zinssatz;
	
	Sparbuch(int a, int b, int c){
		this.Kontonummer = a;
		this.Kapital = b;
		this.Zinssatz = c;
	}
	
	public void zahleEin(int a){		//Kontostand + a
		Kapital += a;
	}
	public void hebeAb(int a){
		Kapital -= a;
	}
	public void verzinse() {
		Kapital *= (Zinssatz/=100);
	}
	public int getErtrag(int a) {		
		Kapital *= Zinssatz;
		return Kapital;
	}
	public int getKontonummer() {
		return Kontonummer;
	}

	public int getKapital() {
		return Kapital;
	}

	public int getZinssatz() {
		return Zinssatz;
	}
	
}
