﻿import java.util.Scanner; //Version 2

class Fahrkartenautomat
{

    public static void main(String[] args){
    	//System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAa");     // Debug. 	I G N O R I E R E N
    	fahrkartenbestellungErfassen();

      /* @SuppressWarnings("resource")
       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       }
       */
    }
    
    public static void fahrkartenbestellungErfassen() {
    	Scanner tastatur = new Scanner(System.in);
        
        double zuZahlenderBetrag; 						//Initialisieren
        double eingezahlterGesamtbetrag = 0;
        double eingeworfeneMünze = 0;
        double rückgabebetrag = 0;
        double kostenab = 2.90;
        double tageskarte = 9.00;
        double kleingrp = 23.50;
        int wahl, fahrkartenanzahl;
        
        System.out.println("Wählen sie ihre Wunschfahrkarte für Berlin AB aus: \n Einzelfahrschein[2,90€] (1)\nTageskarte Regeltarif[9,00€] (2)\nKleingruppen-Tageskarte Regeltarif AB[23,90€] (3)");
        wahl = tastatur.nextInt();
        System.out.println("Bitte geben sie die anzahl and Fahrkarten an:(Maximal 10) ");			/// DEBUG \\\ 
        fahrkartenanzahl = tastatur.nextInt();
        
        if(fahrkartenanzahl > 10 || fahrkartenanzahl < 1) {			//Wenn zu viele Fahrkarten beantragt werden, dann wird sich das zuruecksetzen.
        	stop();													
        }
        
        switch (wahl) {			//Swtch stuff, keine if-else anfragen. Switch ist einfacher
		case 1:
			zuZahlenderBetrag = fahrkartenanzahl * kostenab;
        	fahrkartenBezahlen(zuZahlenderBetrag, eingezahlterGesamtbetrag, eingeworfeneMünze, rückgabebetrag );
			break;
		case 2:
			zuZahlenderBetrag = fahrkartenanzahl * tageskarte;
        	fahrkartenBezahlen(zuZahlenderBetrag, eingezahlterGesamtbetrag, eingeworfeneMünze, rückgabebetrag );
			break;
		case 3:
			zuZahlenderBetrag = fahrkartenanzahl * kleingrp;
        	fahrkartenBezahlen(zuZahlenderBetrag, eingezahlterGesamtbetrag, eingeworfeneMünze, rückgabebetrag );
			break;
		default:
			stop();
			break;
		}
        //zuZahlenderBetrag = fahrkartenanzahl * kosten;
       /* if(fahrkartenanzahl == 1) {																			//TEST / DEBUG \
        	System.out.print("Zu zahlende Fahrkarten: "+ fahrkartenanzahl + "in Euro: "+ zuZahlenderBetrag);
        }*/
        
        //fahrkartenAusgeben(eingezahlterGesamtbetrag);
        tastatur.close();
        
    }
    
    public static void stop() {
    	
    	System.out.println("Fehler. System Fehlerhaft. Falsche Eingabe der Parameter.");		//SEHR Angepisster Fahrkartenautomat. NUR 10 FAHRKARTEN, VERDAMMT
    	fahrkartenbestellungErfassen();
    	
    }
    
    public static void fahrkartenBezahlen(double zuZahlenderBetrag, double eingezahlterGesamtbetrag, double rückgabebetrag, double eingeworfeneMünze) {
    	
    	Scanner tastatur = new Scanner(System.in);
    	eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   System.out.println("Noch zu zahlen: " + (zuZahlenderBetrag - eingezahlterGesamtbetrag) + " Euro");	//Schön zahlen.
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 5 Euro): "); 
     	   eingeworfeneMünze = tastatur.nextDouble();
     	   if(eingeworfeneMünze >5 || eingeworfeneMünze < 1) {
     		   System.out.println("\n\n Fehler. Geld nicht erkannt.");
     		   fahrkartenBezahlen(zuZahlenderBetrag,eingezahlterGesamtbetrag,rückgabebetrag,eingeworfeneMünze);
     	   
     	   }
    
     	   eingezahlterGesamtbetrag += eingeworfeneMünze;
        
        }
        
        tastatur.close();
        fahrkartenAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
    
    }
    
    public static void fahrkartenAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {				
    
    	System.out.println("\nFahrschein wird ausgegeben");	
        for (int i = 0; i < 12; i++)
        {
 			try {
				Thread.sleep(250);
				 System.out.print("=");
			} catch (InterruptedException e) {
		
				e.printStackTrace();
			
			}
           
        }
        
        System.out.println("\n\n");
    	rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
    
    }
    
    public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
    	
    	double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
   
    	if(rückgabebetrag > 0.0)
        {
     	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
         	 rückgabebetrag -= 2.0;
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
         	 rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
         	 rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
         	 rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            { 
         	  System.out.println("10 CENT");
         	 rückgabebetrag -= 0.1;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT"); 
         	 rückgabebetrag -= 0.05;
            }
            
        }
    	System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                "vor Fahrtantritt entwerten zu lassen!\n"+
                "Wir wünschen Ihnen eine gute Fahrt.");
    }
    	
}