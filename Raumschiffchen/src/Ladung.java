import java.util.ArrayList;

public class Ladung {

	private String name;
	private int anzahl;
	Ladung(String name, int anzahl){
		this.anzahl = anzahl;
		this.name = name;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAnzahl() {
		return anzahl;
	}
	public void setAnzahl(int anzahl) {
		this.anzahl = anzahl;
	}
	
}
